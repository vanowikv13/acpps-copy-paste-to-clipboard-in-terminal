#ifndef CLIPBOARD_H
#define CLIPBOARD_H

#include <stdio.h>
#include <limits.h>
#include <X11/Xlib.h>
#include <exception>
#include <string.h>

class Clipboard
{
    Display *display;
    unsigned long color, bufid;
    Window window;

public:
    Clipboard()
    {
        display = XOpenDisplay(NULL);
        color = BlackPixel(display, DefaultScreen(display));
        window = XCreateSimpleWindow(display, DefaultRootWindow(display), 0, 0, 1, 1, 0, color, color);
        bufid = XInternAtom(display, "CLIPBOARD", False);
    }

    bool printClipboard(const char *fmtname)
    {
        char *result;
        unsigned long ressize, restail;
        int resbits;
        unsigned long fmtid = XInternAtom(display, fmtname, False),
            propid = XInternAtom(display, "XSEL_DATA", False),
            incrid = XInternAtom(display, "INCR", False);
        XEvent event;

        XSelectInput(display, window, PropertyChangeMask);
        XConvertSelection(display, bufid, fmtid, propid, window, CurrentTime);
        do
        {
            XNextEvent(display, &event);
        } while (event.type != SelectionNotify || event.xselection.selection != bufid);

        if (event.xselection.property)
        {
            XGetWindowProperty(display, window, propid, 0, LONG_MAX / 4, True, AnyPropertyType,
                &fmtid, &resbits, &ressize, &restail, (unsigned char **)&result);
            if (fmtid != incrid)
                printf("%.*s", (int)ressize, result);
            XFree(result);

            if (fmtid == incrid)
                do
                {
                    do
                    {
                        XNextEvent(display, &event);
                    } while (event.type != PropertyNotify || event.xproperty.atom != propid || event.xproperty.state != PropertyNewValue);

                    XGetWindowProperty(display, window, propid, 0, LONG_MAX / 4, True, AnyPropertyType,
                        &fmtid, &resbits, &ressize, &restail, (unsigned char **)&result);
                    printf("%.*s", (int)ressize, result);
                    XFree(result);
                } while (ressize > 0);
                return true;
        }
        return false;
    }

    void setClipboard(const char *clipValue)
    {
        char *result;
        unsigned long ressize;
        unsigned long targets_atom = XInternAtom(display, "TARGETS", 0),
            text_atom = XInternAtom(display, "TEXT", 0),
            UTF8 = XInternAtom(display, "UTF8_STRING", 1);
        XEvent event;

        XSetSelectionOwner(display, bufid, window, 0);
        if (XGetSelectionOwner(display, bufid) != window)
            return;

        while (true)
        {
            XNextEvent(display, &event);

            switch (event.type)
            {
            case SelectionRequest:
            {
                if (event.xselectionrequest.selection == bufid)
                {
                    XSelectionRequestEvent *xsr = &event.xselectionrequest;
                    XSelectionEvent ev ={ 0 };
                    ev.type = SelectionNotify, ev.display = xsr->display, ev.requestor = xsr->requestor,
                        ev.selection = xsr->selection, ev.time = xsr->time, ev.target = xsr->target, ev.property = xsr->property;
                    if (ev.target == targets_atom)
                        sendEventForSetClip(ev, 4, 32, (const char*)&UTF8);
                    else if (ev.target == 31 || ev.target == text_atom) {
                        sendEventForSetClip(ev, 31, 8, clipValue);
                        return;
                    }
                    else if (ev.target == UTF8) {
                        sendEventForSetClip(ev, UTF8, 8, clipValue);
                        return;
                    }
                    else
                        ev.property = None;
                }
                break;
            }
            case SelectionClear:
                return;
            }
        }
    }

    ~Clipboard()
    {
        XDestroyWindow(display, window);
        XCloseDisplay(display);
    }

private:
    void sendEventForSetClip(XSelectionEvent &ev, int propType, int format, const char * valueToSet)
    {
        int R = XChangeProperty(ev.display, ev.requestor, ev.property, propType, format, PropModeReplace, (const unsigned char*)valueToSet, strlen(valueToSet));
        if ((R & 2) == 0)
            XSendEvent(display, ev.requestor, 0, 0, (XEvent *)&ev);
    }
};

#endif //!CLIPBOARD_H