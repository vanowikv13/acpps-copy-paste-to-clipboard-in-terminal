#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>

#include "clipboard.h"

int main(int argc, char **argv)
{
    Clipboard *clipboard = new Clipboard();

    if (isatty(fileno(stdin))) {
        if (!clipboard->printClipboard("UTF8_STRING"))
            clipboard->printClipboard("STRING");
    }
    else
    {
        char *all = NULL;
        char *line = NULL;
        size_t n = 0, result = 0, s = 0;

        while ((result = getline(&line, &n, stdin)) != -1)
        {
            all = (char *)realloc(all, s + result + 1);
            strcpy(all + s, line);
            s += result;

            n = 0;
            free(line);
        }

        clipboard->setClipboard(all);
        free(all);
    }

    delete clipboard;
    return 0;
}
